#!/usr/bin/python


class FilterModule(object):

    def filters(self):
        return {'username': self.username}

    def username(self, name):
        the_name = name.split(' ')
        username = the_name[0][0] + the_name[1]
        return username.lower()
