#!/usr/bin/python


class FilterModule(object):

    def filters(self):
        return {'columnify': self.columnify}

    def columnify(self, mounts):
        fields = ['device', 'mount', 'fstype', 'options', 'freq', 'passno']
        max_len = {}
        for mount in mounts:
            for field in fields:
                the_len = len(mount[field])
                if field not in max_len or the_len > max_len[field]:
                    max_len[field] = the_len

        return '%-{}s  %-{}s  %-{}s  %-{}s  %-{}s %-{}s'.format(
                 max_len['device'],
                 max_len['mount'],
                 max_len['fstype'],
                 max_len['options'],
                 max_len['freq'],
                 max_len['passno']
               )
